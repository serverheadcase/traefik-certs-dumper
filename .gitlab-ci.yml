---
# Copyright (c) 2019, PhysK
# All rights reserved.

image: docker:stable
variables:
  DOCKER_DRIVER: overlay2
  CI_REGISTRY_URL: "$CI_REGISTRY/$CI_PROJECT_PATH"
  BUILDX_VERSION: "v0.7.1"
  DOCKER_BUILDKIT: 1
  # yamllint disable-line rule:line-length
  BUILDX_URL: https://github.com/docker/buildx/releases/download/${BUILDX_VERSION}/buildx-${BUILDX_VERSION}.linux-amd64
  BUILD_PLATFORMS: linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6
  LATEST_TAG: $CI_REGISTRY_URL:latest
  BRANCH_TAG: $CI_REGISTRY_URL:$CI_COMMIT_REF_NAME

services:
  - docker:dind

stages:
  - preflight
  - build

# Generic preflight template
.preflight: &preflight
  stage: preflight
  only:
    - main

.build: &build
  image: docker:latest
  stage: build
  services:
    - name: docker:dind
      command: ["--experimental"]
  before_script:
    - apk add curl
    - mkdir -vp ~/.docker/cli-plugins/
    - curl --silent -L $BUILDX_URL > ~/.docker/cli-plugins/docker-buildx
    - chmod a+x ~/.docker/cli-plugins/docker-buildx
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker buildx create --use
    - docker buildx inspect --bootstrap

# Shell Check
shellcheck:
  <<: *preflight
  image:
    name: koalaman/shellcheck-alpine:stable
    entrypoint: [""]
  before_script:
    - shellcheck --version
    - apk --no-cache add grep
    - |
      find . -type f -print0 | \
        xargs -0 sed -i 's:#!/usr/bin/with-contenv bash:#!/bin/bash:g'
  script:
    - |
      for file in $(grep -IRl "#\!\(/bin/\|/usr/bin/\)" "root/"); do
        if ! shellcheck $file; then
          export FAILED=1
        else
          echo "$file OK"
        fi
      done
      if [ "${FAILED}" = "1" ]; then
        exit 1
      fi

# Yaml Lint
yamllint:
  <<: *preflight
  image: sdesbure/yamllint
  before_script:
    - yamllint --version
  script:
    - yamllint .

# JSON Lint
jsonlint:
  <<: *preflight
  image: sahsu/docker-jsonlint
  before_script:
    - jsonlint --version || true
  script:
    - |
      for file in $(find . -type f -name "*.json"); do
        if ! jsonlint -q $file; then
          export FAILED=1
        else
          echo "$file OK"
        fi
      done
      if [ "${FAILED}" = "1" ]; then
        exit 1
      fi

# Markdown Lint
markdownlint:
  <<: *preflight
  image:
    name: ruby:alpine
    entrypoint: [""]
  before_script:
    - gem install mdl
    - mdl --version
  script:
    - mdl --style all --warnings .

# Build Dev Branch
build-docker-dev:
  <<: *build
  script:
    # yamllint disable-line rule:line-length
    - docker buildx build --push --platform ${BUILD_PLATFORMS} -t ${BRANCH_TAG} .
    - docker manifest inspect ${BRANCH_TAG}
  except:
    - main

# Build main Branch
build-docker-master:
  <<: *build
  script:
    # yamllint disable-line rule:line-length
    - docker buildx build --push --platform ${BUILD_PLATFORMS} -t ${LATEST_TAG} .
    - docker manifest inspect ${LATEST_TAG}
  only:
    - main
