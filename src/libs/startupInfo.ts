import { argv } from '../app';

export const startupInfo = async () => {
  console.log(`########################################`);
  console.log(`# Traefik Certificate Extractor v1`);
  console.log(`########################################`);
  console.log(`# inputFile: ${argv.inputFile}`);
  console.log(`# outputDir: ${argv.outputDir}`);
  console.log(`# watch: ${argv.watch ? 'Yes' : 'No'} `);
  console.log(`########################################`);
};
