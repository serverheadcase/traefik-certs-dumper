import * as path from 'path';
import * as fs from 'fs-extra';
import { Domain } from '../types/acmeFile';
import { flat_dir } from '../app';

export const create_flat_certificates = async (
  private_key: string,
  full_chain: string,
  chain: string,
  domains: Domain
) => {
  const domain = [];

  domain.push(domains.main);

  if (domains.sans !== null && domains.sans !== undefined) {
    for (const san of domains.sans) {
      domain.push(san);
    }
  }

  for (const domain_path of domain) {
    await fs.writeFile(
      path.resolve(flat_dir, `${domain_path}.key`),
      private_key
    );
    await fs.writeFile(
      path.resolve(flat_dir, `${domain_path}.crt`),
      full_chain
    );
    await fs.writeFile(
      path.resolve(flat_dir, `${domain_path}.chain.pem`),
      chain
    );
    console.log(`Extracted Flat Certificate for ${domain_path}`);
  }
};
