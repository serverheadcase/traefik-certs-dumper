import { PathLike, readJSON } from 'fs-extra';
import { acmeFile } from '../types/acmeFile';
import { create_flat_certificates } from './create_flat_certificates';
import { create_certificates } from './create_certificates';

export const processAcme = async (acme_file: PathLike) => {
  const acme: acmeFile = await readJSON(acme_file.toString());

  // eslint-disable-next-line guard-for-in
  for (const resolver in acme) {
    const res = acme[resolver];
    if (res.Certificates) {
      for (const cert of res.Certificates) {
        const priv_key = Buffer.from(cert.key, 'base64').toString('utf-8');
        const full_chain = Buffer.from(cert.certificate, 'base64').toString(
          'utf-8'
        );
        const matches = full_chain.match(
          /(-----BEGIN CERTIFICATE-----\n[A-Za-z0-9\n+/]+={0,2}\n-----END CERTIFICATE-----)/g
        );
        let small_cert = '';
        let chain = '';
        if (matches !== null) {
          // eslint-disable-next-line prefer-destructuring
          small_cert = matches[0];
          chain = ''.concat(matches[1], '\n', matches[2]);
        }
        await create_certificates(
          priv_key,
          full_chain,
          chain,
          small_cert,
          cert.domain
        );
        await create_flat_certificates(
          priv_key,
          full_chain,
          chain,
          cert.domain
        );
      }
    }
  }
};
