import * as path from 'path';
import * as fs from 'fs-extra';
import { Domain } from '../types/acmeFile';
import { certs_dir } from '../app';

export const create_certificates = async (
  private_key: string,
  full_chain: string,
  chain: string,
  certificate: string,
  domains: Domain
) => {
  const domain = [];
  domain.push(domains.main);

  if (domains.sans !== null && domains.sans !== undefined) {
    for (const san of domains.sans) {
      domain.push(san);
    }
  }
  for (const domain_path of domain) {
    const cert_path = path.resolve(certs_dir, domain_path);
    await fs.ensureDir(cert_path);
    await fs.writeFile(path.resolve(cert_path, 'privkey.pem'), private_key);
    await fs.writeFile(path.resolve(cert_path, 'cert.pem'), certificate);
    await fs.writeFile(path.resolve(cert_path, 'chain.pem'), chain);
    await fs.writeFile(path.resolve(cert_path, 'fullchain.pem'), full_chain);
    console.log(`Extracted Certificate for ${domain_path}`);
  }
};
