export interface Domain {
  main: string;
  sans?: string[] | null;
}
export interface CertificatesEntity {
  domain: Domain;
  certificate: string;
  key: string;
  Store: string;
}

export interface ExternalAccountBinding {
  payload: string;
  protected: string;
  signature: string;
}

export interface Body {
  status: string;
  contact?: string[] | null;
  termsOfServiceAgreed: boolean;
  orders: string;
  externalAccountBinding: ExternalAccountBinding;
}

export interface Registration {
  body: Body;
  uri: string;
}

export interface Account {
  Email: string;
  Registration: Registration;
  PrivateKey: string;
  KeyType: string;
}

export interface Resolver {
  Account: Account;
  Certificates?: CertificatesEntity[] | null;
}

export interface acmeFile {
  [key: string | number]: Resolver;
}
