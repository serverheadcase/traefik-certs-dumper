import * as path from 'path';
import * as fs from 'fs-extra';
import * as chokidar from 'chokidar';
import yargs from 'yargs';

import { processAcme } from './libs/processAcme';
import { startupInfo } from './libs/startupInfo';

export const argv = yargs(process.argv.slice(2))
  .options({
    inputFile: { type: 'string', default: '/data/acme.json' },
    outputDir: { type: 'string', default: '/certs' },
    watch: { type: 'boolean', default: true },
  })
  .parseSync();

export const acme_file = argv.inputFile;
export const flat_dir = path.resolve(argv.outputDir, 'certs_flat');
export const certs_dir = path.resolve(argv.outputDir, 'certs');

const main = async () => {
  await startupInfo();
  await fs.ensureDir(certs_dir);
  await fs.emptydir(flat_dir);

  await processAcme(acme_file);

  const watcher = chokidar.watch(acme_file);
  watcher.on('change', async (filePath) => {
    await processAcme(filePath);
    console.log('Watching File for changes');
  });
  console.log('Watching File for changes');
};

main();
