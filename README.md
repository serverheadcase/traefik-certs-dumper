# Traefik Cert Extractor

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![pipeline status](https://gitlab.com/serverheadcase/traefik-certs-dumper/badges/main/pipeline.svg)](https://gitlab.com/serverheadcase/traefik-certs-dumper/-/commits/main)

## Features

---

- File watch
- Extract Full Certificates
- Extract Flat Certificates

## How To

---

We Build multi-arch docker images so when pull a tag
it will give you the correct arch for your system

```bash
docker run -d \
-v /path/to/traefik/acme.json:/data/acme.json \
-v /path/to/store/certs/:/certs \
registry.gitlab.com/serverheadcase/traefik-certs-dumper
```
