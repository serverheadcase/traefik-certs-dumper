FROM node:14-alpine as builder

ENV PYTHONUNBUFFERED=1

RUN echo "https://dl-cdn.alpinelinux.org/alpine/v$(cut -d'.' -f1,2 /etc/alpine-release)/main/" > /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$(cut -d'.' -f1,2 /etc/alpine-release)/community/" >> /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing/" >> /etc/apk/repositories && \
    apk update && \
    apk add --no-cache build-base alpine-sdk python3 && ln -sf python3 /usr/bin/python && \
    python3 -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools
RUN mkdir -p /app/src
COPY src/ /app/src
COPY .eslintrc /app
COPY package.json /app
COPY tsconfig.json /app
COPY yarn.lock /app

RUN cd /app && \
    ls -lsa && \
    yarn install
run cd /app && \
    yarn build:typescript

FROM node:14-alpine

RUN mkdir -p /app
RUN apk update && apk add bash
COPY --from=builder /app/ /app
VOLUME "/data"
VOLUME "/certs"

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/yarn", "start:node"]
